# Create a Python class that reads in an excel file and sends it to Azure OpenAI to identify the epci an/or requirements column and then send each row in the column to Azure 
# OpenAi to have it generate a user story. The user story should then be store in a new column title: "User Story"
import json

import pandas as pd
import openai
import os
import sys
import traceback
import logging
import time
from datetime import datetime
from dotenv import load_dotenv
from pathlib import Path
from openai.api_resources import engine
from openai import Engine

# Load environment variables
env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

# Set up logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# Create handlers
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.DEBUG)
c_handler.setFormatter(formatter)
logger.addHandler(c_handler)

# Set up OpenAI
openai.api_key = os.getenv('OPENAI_API_KEY')
engine_id = os.getenv('OPENAI_ENGINE_ID')
engine = Engine(engine_id)

# Set up Azure
# azure_key = os.getenv('AZURE_KEY')
# azure_endpoint = os.getenv('AZURE_ENDPOINT')
# azure_client = TextAnalyticsClient(endpoint=azure_endpoint, credential=azure_key)

# Load file from Excel
df = pd.read_excel('data.xlsx')
# print(df.head())

# Create a Python class to handle generation of user stories from epics

class UserStoryGenerator:

    def __init__(self, df):
        self.df = df
        self.epic = None
        self.requirement = None
        self.user_story = None
        self.user_story_list = []
        self.config = self.load_config()

    @staticmethod
    def load_config():
        with open('openai_config.ini') as f:
            data = f.read()
        config = json.loads(data)
        return config

    def generate_user_story(self):
        for i in range(len(self.epic)):
            self.user_story_list.append(engine.complete(
                prompt=f"Given the epic: {self.epic[i]} and the requirement: {self.requirement[i]}, write a user story that describes the requirement in more detail.\n\nUser Story: {self.user_story[i]}",
                max_tokens=60,
                temperature=0.2,




