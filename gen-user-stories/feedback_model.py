__author__ = "Chris Steel"
__copyright__ = "Copyright 2024, Syntheticore Corporation"
__credits__ = ["Chris Steel"]
__date__ = "1/25/2024"
__license__ = "Syntheticore Confidential"
__version__ = "1.0"
__email__ = "csteel@syntheticore.com"
__status__ = "Production"

import numpy as np
import torch
import pandas as pd
from tqdm import tqdm


class FeedbackModel:
    def __init__(self):
        self._feedback = None
        self.adapter_query_embeddings = torch.Tensor(np.array(adapter_query_embeddings))
        self.adapter_doc_embeddings = torch.Tensor(np.array(adapter_doc_embeddings))
        self.adapter_labels = torch.Tensor(np.expand_dims(np.array(adapter_labels),1))
        self.mat_size = len(adapter_query_embeddings[0])
        self.adapter_matrix = torch.randn(mat_size, mat_size, requires_grad=True)
        self.best_matrix = None

    def train(self) -> None:
        """Train the model."""
        min_loss = float('inf')
        for epoch in tqdm(range(100)):
            for query_embedding, document_embedding, label in dataset:
                loss = mse_loss(query_embedding, document_embedding, adapter_matrix, label)

                if loss < min_loss:
                    min_loss = loss
                    best_matrix = adapter_matrix.clone().detach().numpy()

                loss.backward()
                with torch.no_grad():
                    adapter_matrix -= 0.01 * adapter_matrix.grad
                    adapter_matrix.grad.zero_()

        print(f"Best loss: {min_loss.detach().numpy()}")