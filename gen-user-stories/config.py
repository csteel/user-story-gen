import configparser
from pathlib import Path


class Config:

    def __init__(self, config_file_path="openai_config.ini"):
        self.config = configparser.ConfigParser()
        self.config_file_path = config_file_path
        self.load()

    def load(self):
        file = Path(self.config_file_path)
        if file.exists():
            self.config.read(self.config_file_path)
        else:
            raise FileNotFoundError(f"File not found: {self.config_file_path}")

    def get_openai_config(self):
        return dict(self.config.items("openai"))

    def get_azure_openai_config(self):
        return dict(self.config.items("azure_openai"))


if __name__ == "__main__":
    config = Config()
    openai_config = config.get_openai_config()
    print(f"OpenAI: {openai_config}")
    azure_config = config.get_azure_openai_config()
    print(f"\n\nAzure OpenAI: {azure_config}")
