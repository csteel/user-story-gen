__author__ = "Chris Steel"
__copyright__ = "Copyright 2023, Syntheticore Corporation"
__credits__ = ["Chris Steel"]
__date__ = "12/6/2023"
__license__ = "Syntheticore Confidential"
__version__ = "1.0"
__email__ = "csteel@syntheticore.com"
__status__ = "Production"

import os
import warnings

import openai
import importlib_metadata
from azure.identity import ClientSecretCredential
from langchain_community.llms import AzureOpenAI
from typing import List
from openai import OpenAI

from config import Config

class NewAzureOpenAI(AzureOpenAI):
    stop: List[str] = None
    @property
    def _invocation_params(self):
        params = super()._invocation_params
        # fix InvalidRequestError: logprobs, best_of and echo parameters are not available on gpt-35-turbo model.
        params.pop('logprobs', None)
        params.pop('best_of', None)
        params.pop('echo', None)
        #params['stop'] = self.stop
        return params


class OpenAIFactory:
    OPENAI = "openai"
    AZURE_OPENAI = "azure_openai"

    @staticmethod
    def create_instance(instance_type=AZURE_OPENAI, temperature=None):

        # Azure OpenAI
        if instance_type == OpenAIFactory.AZURE_OPENAI:
            # Get Azure Text Analytics credentials
            api_type = "azure_ad"
            config = Config().get_azure_openai_config()

            credential = ClientSecretCredential(config["tenant_id"], config["service_principal"], config["service_principal_secret"])

            openai_version = importlib_metadata.version("openai")
            if int(openai_version[0]) > 0:
                print("OpenAI version > 1")
                #vtoken_provider = get_bearer_token_provider(credential, "https://cognitiveservices.azure.com/.default")

                os.environ["OPENAI_API_TYPE"] = config["api_type"]
                os.environ["OPENAI_API_VERSION"] = config["api_version"]
                os.environ["AZURE_OPENAI_ENDPOINT"] = config["api_endpoint"]
                os.environ["AZURE_OPENAI_API_KEY"] = credential.get_token("https://cognitiveservices.azure.com/.default").token

                # Import Azure OpenAI
                # from langchain.llms import AzureOpenAI

                # Create an instance of Azure OpenAI
                # Replace the deployment name with your own
                _llm = openai.AzureOpenAI(
                    azure_deployment=config["deployment_id"]
                )
                # Generate text using OpenAI API
                response = _llm.chat.completions.create(
                    model=config["model_name"],
                    messages=[{"role": "user","content": "Say this is a test",}],
                    max_tokens=5
                )

                # Print the generated text
                print(response.choices[0].message.content)
            else:
                print("OpenAI version == 0")
                os.environ["OPENAI_API_TYPE"] = "azure"
                os.environ["OPENAI_API_VERSION"] = config["api_version"]
                os.environ["OPENAI_API_KEY"] = credential.get_token("https://cognitiveservices.azure.com/.default").token
                os.environ["OPENAI_API_BASE"] = config["api_endpoint"]

                warnings.filterwarnings("ignore", category=UserWarning)
                _llm = NewAzureOpenAI(
                    deployment_name=config["deployment_id"],
                    model_name=config["model_name"],
                    api_type=api_type  # need to specify the type since using ClientSecretCredential
                )
                warnings.filterwarnings("default")
            return _llm
        # OpenAI
        elif instance_type == "openai":
            # Get OpenAI API key
            config = Config().get_openai_config()
            openai.api_key = config["api_key"]
            if not os.getenv("OPENAI_API_KEY"):
                os.environ["OPENAI_API_KEY"] = openai.api_key
            if not temperature:
                temperature = config["temperature"]

            _llm = OpenAI()
            """
            try:
                _llm("hello")
            except Exception as ex:
                raise ConnectionError("Can't connect to OpenAI? Error: " + str(ex))
            """
        else:
            raise ValueError("Invalid instance type specified")

        return _llm


if __name__ == "__main__":

    llm = OpenAIFactory.create_instance(OpenAIFactory.OPENAI)
    chat_completion = llm.chat.completions.create(
        model="gpt-4",
        messages=[
            {
                "role": "user",
                "content": "How is Azure machine learning different than Azure OpenAI?",
            },
        ]
    )
    print(chat_completion.model_dump_json(indent=2))

