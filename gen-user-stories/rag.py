__author__ = "Chris Steel"
__copyright__ = "Copyright 2024, Syntheticore Corporation"
__credits__ = ["Chris Steel"]
__date__ = "1/26/2024"
__license__ = "Syntheticore Confidential"
__version__ = "1.0"
__email__ = "csteel@syntheticore.com"
__status__ = "Production"

from llm_factory import OpenAIFactory

class RAG:

    def __init__(self, model: str = "gpt-4"):
        self.model = model
        self.openai_client = OpenAIFactory.create_instance(instance_type=OpenAIFactory.OPENAI)

    def rag(self, query, retrieved_documents, model=None):
        if not model:
            model = self.model
        information = "\n\n".join(retrieved_documents)  # Join the retrieved documents to use as supplemental information for LLM
        messages = [
            {
                "role": "system",
                "content": "You are a helpful expert technical marketing research assistant. Your users are asking questions about information contained in company literature."
                           "You will be shown the user's question, and the relevant information from the literature. Answer the user's question in one sentence using the provided information but also use any other information you have on the topic."
            },
            {
                "role": "user", "content": f"Question: {query}. \n Information: {information}"
            }
        ]

        response = self.openai_client.chat.completions.create(
            model=model,
            messages=messages,
        )
        content = response.choices[0].message.content
        return content


if __name__ == "__main__":
    rag = RAG(model='gpt-4-turbo-preview')
    query = "What is the best way to get started with the product?"
    retrieved_documents = ["The best way to get started with the product is to search the web.",
                           "A good way to get started with the product is to read the documentation.",
                           "An alternative way to get started with the product is to ask a friend."]
    content = rag.rag(query, retrieved_documents)
    print(content)
