# Introduction

User stories are short and simple descriptions of a feature or
functionality from the perspective of an end user. They are often used
in agile software development to capture the needs and expectations of
the customers and stakeholders. User stories typically follow a format
like \"As a \<role\>, I want \<goal\>, so that \<benefit\>\".

However, writing user stories can be a time-consuming and tedious task,
especially when there are many epics and requirements to cover. Epics
are large and complex user stories that span multiple features or
functionalities. Requirements are detailed specifications of what the
system should do or how it should behave. JIRA is a popular software
development tool that allows users to create and manage epics and
requirements as issues.

User Story Generator is a project that aims to automate the process of
creating user stories from JIRA epics and requirements. It uses natural
language processing and machine learning techniques to analyze the epics
and requirements and generate user stories that are concise, relevant,
and consistent.

# Features

-   Reads epics and requirements from an Excel download from JIRA

-   Future: Connects to JIRA using API and fetches the epics and
    requirements as input.

-   Parses and extracts the key information from the epics and
    requirements, such as the role, goal, benefit, and acceptance
    criteria.

-   Generates user stories that follow the standard format and include
    the necessary details.

-   Outputs the user stories along with the epics and test cases into
    Excel file format for review

-   Future: Output the stories back into JIRA and assign to a reviewer.

-   Future: Allows users to customize the generation process by setting
    parameters such as the number of user stories, the level of detail,
    and the tone of voice.

# Generative AI for Requirements Translation

The project will use IQVIA's private Azure OpenAI instance and the code
is managed in Gitlab.

## Data Collection

-   A set of actual requirements and user stories in their native
    formats were requested from Patel, Ajay and Dan.

-   Test cases in native format were also requested.

-   Patel, Ajay confirmed that Dan can provide more examples.

-   A document named "GenAI UserStories" was shared by Patel, Ajay.

-   More requirements and user stories were requested from Peters, Dan
    to test against.

-   Peters, Dan promised to provide some requirements and stories by
    Wednesday.

-   A mapping of requirements to user stories was requested.

-   Architecture or design documentation of the application the stories
    are for was also requested.

-   Peters, Dan suggested setting up a call to explain rather than
    emailing.

## POC with DevOps Team

-   A POC is being worked on with the DevOps team on using Generative AI
    to translate requirements into user stories.

-   Access to Confluence and JIRA was requested from Kapadia, Jay to
    pull over requirements and user stories.

-   Kapadia, Jay informed that they do not write requirements, only
    stories, and that these are created in meetings without transcripts.

-   Access to requirements and user stories was then requested from
    Namasivayam, Senthilvelan.

-   Serraj Andaloussi, Meryem assisted in getting access to JIRA and the
    OER project.

## Meetings and Discussions

-   An invitation to a Microsoft Teams meeting to discuss identifying
    requirements, stories, acceptance criteria, and test cases was
    accepted.

-   The meeting was organized by Peters, Dan and the agenda was shared.

-   The meeting can be joined via computer, mobile app, or room device
    and the Meeting ID and Passcode were provided.

-   Alternatively, the meeting can be joined via a video conferencing
    device or by calling in (audio only).

-   The email thread also included instructions on how to join the
    meeting, download Teams, and find a local number.

## Enterprise DevOps Access Management

-   A request item RITM21276855 was opened on behalf by IQVIA Request
    Management for Enterprise DevOps Access Management for various
    platform tools.

-   The request was opened on 30-Nov-2023 and the target date for
    completion is 07-Dec-2023.

-   The details of the request can be viewed by clicking the provided
    link in the email.

-   The Global IT Service Desk has provided contact information for
    assistance, including phone numbers and a link to submit a ticket.
