__author__ = "Chris Steel"
__copyright__ = "Copyright 2023, Syntheticore Corporation"
__credits__ = ["Chris Steel"]
__date__ = "12/6/2023"
__license__ = "Syntheticore Confidential"
__version__ = "1.0"
__email__ = "csteel@syntheticore.com"
__status__ = "Production"

import secrets
from datetime import datetime

from azure.identity import ClientSecretCredential
from azure.mgmt.resource import ResourceManagementClient
from azure.mgmt.authorization import AuthorizationManagementClient
from azure.graphrbac.models import PasswordCredential


class AzureServiceManager:
    """ Manages Azure services """

    def __init__(self, tenant_id, client_id, client_secret, subscription_id):
        self.credentials = ClientSecretCredential(tenant_id, client_id, client_secret)
        self.resource_client = ResourceManagementClient(self.credentials, subscription_id)
        self.auth_client = AuthorizationManagementClient(self.credentials, tenant_id)
        # ... additional initializations ...

    def get_users_in_group(self, group_id):
        # Logic to fetch users from the specified AD group
        pass

    @staticmethod
    def generate_random_secret():
        # Generate a 40-character hexadecimal string
        return secrets.token_hex(20)  # 20 bytes = 40 hex characters

    def create_service_principal(self, user_id):
        pass

    def create_service_principal_secret(self, service_principal_id):
        # Generate a secure random secret
        secret_value = self.generate_random_secret()

        # Define the validity period for the secret
        end_date = datetime.datetime.utcnow() + datetime.timedelta(days=3650)  # 10 year validity

        # Create a new secret
        password_credential = PasswordCredential(
            start_date=datetime.datetime.utcnow(),
            end_date=end_date,
            key_id=str(datetime.datetime.utcnow().timestamp()),  # Generate a unique key id
            value=secret_value  # Use the generated secret value
        )

        # Add the secret to the service principal
        self.auth_client.service_principals.create_password_credential(
            service_principal_id=service_principal_id,
            parameters=password_credential
        )

        return secret_value


if __name__ == "__main__":
    # azm = AzureServiceManager()
    print(AzureServiceManager.generate_random_secret())
