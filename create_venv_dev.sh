#!/bin/bash

source~/miniconda/etc/profile.d/conda.sh
export env_name=user-story-gen
conda update conda -y
conda init --all
conda deactivate
conda remove -y --all -n $env_name 
conda create -y --no-default-packages -n $env_name python=3.10
conda activate $env_name 
pip install --upgrade pip
pip install --no-cache-dir -r requirements.txt --extra-index-url=https://pypi.nvidia.com
python -m ipykernel install --user --name=$env_name
